import pandas as pd
from sklearn.model_selection import train_test_split as Split
from sklearn.metrics import mean_squared_error as MSE
import numpy as np
from sklearn.model_selection import ParameterGrid
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import export_graphviz as treeToDOT
from sklearn.ensemble import BaggingRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor
from xgboost import XGBRegressor
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV



def main():
    path = 'Data/ccpp.xlsx'
    df = pd.read_excel(path)

    y = df.PE
    X = df.drop(["PE"], axis=1)
    X_train, X_test, y_train, y_test = Split(X, y, test_size = 0.2, random_state=9)
    data = (X_train, X_test, y_train, y_test)

    ## Optimal Depth for regression tree:
    DecisionTree, max_depth = find_optimal_depth(data)
    print('Best depth for sklearn RegressionTree(): ', max_depth)
    treeToDOT(DecisionTree, out_file='tree.dot')


    #Bagging
    depth = max_depth + 3
    Model = DecisionTreeRegressor(max_depth=depth)
    BaggingDecisionTree = BaggingRegressor(Model)
    BaggingDecisionTree.fit(X_train, y_train)

    print("Decision tree regressor MSE:")
    print(getMetric(MSE, (y_test, DecisionTree.predict(X_test))))
    print("and with bagging:")
    print(getMetric(MSE, (y_test, BaggingDecisionTree.predict(X_test))))

    #Random Forest
    ForestRegressor = RandomForestRegressor(n_estimators=100, max_depth=depth+1)
    ForestRegressor.fit(X_train, y_train)
    print("Random Forest Regressor MSE:")
    print(getMetric(MSE, (y_test, ForestRegressor.predict(X_test))))
    print("Feature Importance:")
    print(ForestRegressor.feature_importances_)

    #Boosting
    BoostingDecisionTree = GradientBoostingRegressor(max_depth=depth, validation_fraction=0.2, n_iter_no_change=10, tol=0.001)
    BoostingDecisionTree.fit(X_train, y_train)
    print("Gradient boosting regressor MSE:")
    print(getMetric(MSE, (y_test, BoostingDecisionTree.predict(X_test))))


    #Stacking
    Model1 = LinearRegression().fit(X_train, y_train)
    Model2 = SVR().fit(X_train, y_train)
    Model3 = KNeighborsRegressor().fit(X_train, y_train)
    Model4 = RandomForestRegressor().fit(X_train, y_train)
    Models = [Model1, Model2, Model3, Model4]
    train_predictions = np.array([mod.predict(X_train) for mod in Models]).T
    test_predictions = np.array([mod.predict(X_test) for mod in Models]).T
    newX_test = np.concatenate((X_test, test_predictions), axis=1)
    newX_train = np.concatenate((X_train, train_predictions), axis=1)


    StackingModel = DecisionTreeRegressor(max_depth=depth).fit(newX_train, y_train)
    print("Stacking MSE:")
    print(getMetric(MSE, (y_test, StackingModel.predict(newX_test))))

    #XGBoost
    parameters_for_testing = {
       'colsample_bytree': [0.4, 0.6, 0.8],
       'gamma': [0, 0.03, 0.1, 0.3],
       'min_child_weight': [1.5, 6, 10, 0.5, 1.0, 3.0, 5.0],
       'learning_rate': [0.1, 0.07],
       'max_depth': [3, 5, 7, 9],
       'n_estimators': [100, 300, 10, 1000],
       'reg_alpha': [1e-5, 1e-2,  0.75],
       'reg_lambda': [1e-5, 1e-2, 0.45, 0.1, 1.0, 5.0, 10.0],
       'subsample': [0.1, 0.3, 0.6, 0.95]
    }

    XG = RandomizedSearchCV(estimator=XGBRegressor(verbosity=0), param_distributions=parameters_for_testing, n_jobs=6, verbose=0, refit=True)

    XG.fit(X_train, y_train)
    print("XGBoost MSE: ")
    print(getMetric(MSE, (y_test, XG.predict(X_test))))


def find_optimal_depth(data):
    X_train, X_test, y_train, y_test = data
    Model = DecisionTreeRegressor()
    grid = np.arange(100)+1
    param_grid = {'max_depth': grid}
    metric = MSE
    best_metric = float('inf')
    for g in ParameterGrid(param_grid):
        Model.set_params(**g)
        Model.fit(X_train, y_train)
        y_predict = Model.predict(X_test)
        val = getMetric(metric,(y_test, y_predict))
        if val < best_metric:
            best_grid = g
            best_metric = val

    Model.set_params(**best_grid)
    Model.fit(X_train, y_train)
    return Model, Model.get_params()['max_depth']


def getMetric(metric, data):
    y_true, y_pred = data
    return metric(y_true, y_pred)


if __name__ == '__main__':
    main()